package my.cardmatchgame;

import android.util.Log;

public class CardReadyThread extends Thread {
	CardGameView view;

	public CardReadyThread(CardGameView view) {
		this.view = view;
	}

	public void run() {
		while (true) {
			if (view.getState() != CardGameView.STATE_HINT) {
				continue;
			}
			Log.i("Ready", "Open cards");
			for (int i = 0; i < 6; i++) {
				view.cards[i].state = Card.CARD_SHOW;
			}

			view.postInvalidate();

			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
			}

			Log.i("Ready", "Close cards");
			for (int i = 0; i < 6; i++) {
				view.cards[i].state = Card.CARD_CLOSE;
			}

			view.setState(CardGameView.STATE_GAME);
			view.startTimer();
			view.postInvalidate();
		}
	}

}

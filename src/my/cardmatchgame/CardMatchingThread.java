package my.cardmatchgame;

public class CardMatchingThread extends Thread {
	CardGameView view;

	public CardMatchingThread(CardGameView view) {
		this.view = view;
	}
	
	public void run() {
		while (true) {
		if (view.selectedCard1 == null || view.selectedCard2 == null) {
			continue;
		}

		if (view.selectedCard1.color == view.selectedCard2.color) {
			view.selectedCard1.state = Card.CARD_MATCHED;
			view.selectedCard2.state = Card.CARD_MATCHED;
		} else {
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
			}

			view.selectedCard1.state = Card.CARD_CLOSE;
			view.selectedCard2.state = Card.CARD_CLOSE;
		}

		view.selectedCard1 = null;
		view.selectedCard2 = null;
		
		if (view.isAllMatched()) {
			view.endGame();
		}
		

		view.postInvalidate();
		}
	}
}

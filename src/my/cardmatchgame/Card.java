package my.cardmatchgame;

import android.graphics.Rect;

public class Card {
	public static final int CARD_SHOW = 0; // Show color when before game start.
	public static final int CARD_CLOSE = 1; // Closed.
	public static final int CARD_PLAYEROPEN = 2; // Temporarily open by user.
	public static final int CARD_MATCHED = 3; // Permanently open by matched.

	public static final int COLOR_RED = 0;
	public static final int COLOR_GREEN = 1;
	public static final int COLOR_BLUE = 2;

	int color;
	int state;

	int x;
	int y;
	Rect thisBox;

	public Card(int color, int x, int y) {
		state = CARD_SHOW;
		this.color = color;
		this.x = x;
		this.y = y;

		thisBox = new Rect(x, y, x + 150, y + 250);
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public boolean isContains(int x, int y) {
		return thisBox.contains(x, y);
	}
}

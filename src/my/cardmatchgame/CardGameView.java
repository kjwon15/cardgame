package my.cardmatchgame;

import java.util.Arrays;
import java.util.Collections;
import java.util.Timer;
import java.util.TimerTask;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

public class CardGameView extends View {

	public static final int STATE_READY = 0;
	public static final int STATE_HINT = 1;
	public static final int STATE_GAME = 2;
	public static final int STATE_END = 3;

	private int state = STATE_READY;

	Card selectedCard1 = null;
	Card selectedCard2 = null;

	Bitmap backgroundImage;
	Bitmap backside;

	Bitmap cardRed;
	Bitmap cardBlue;
	Bitmap cardGreen;

	Card cards[];

	long startedAt;
	long endedAt;
	Timer timer;

	MediaPlayer soundBackground;
	SoundPool soundPool;
	int soundFlipId;

	public CardGameView(Context context) {
		super(context);

		backgroundImage = BitmapFactory.decodeResource(getResources(),
				R.drawable.background);
		backside = BitmapFactory.decodeResource(getResources(),
				R.drawable.backside);

		cardRed = BitmapFactory.decodeResource(getResources(),
				R.drawable.front_red);
		cardGreen = BitmapFactory.decodeResource(getResources(),
				R.drawable.front_green);
		cardBlue = BitmapFactory.decodeResource(getResources(),
				R.drawable.front_blue);

		cards = new Card[6];
		SetCardShuffle();

		CardMatchingThread matchnigThread = new CardMatchingThread(this);
		matchnigThread.start();

		CardReadyThread readyThread = new CardReadyThread(this);
		readyThread.start();

		soundBackground = MediaPlayer.create(context, R.raw.duelist);
		soundBackground.setLooping(true);
		soundBackground.start();
		
		soundPool = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
		soundFlipId = soundPool.load(context, R.raw.flip, 1);
	}

	private void SetCardShuffle() {

		Integer[] colors = { Card.COLOR_RED, Card.COLOR_RED, Card.COLOR_GREEN,
				Card.COLOR_GREEN, Card.COLOR_BLUE, Card.COLOR_BLUE, };

		Collections.shuffle(Arrays.asList(colors));

		for (int i = 0; i < 6; i++) {
			int x = i / 2;
			int y = i % 2;
			cards[i] = new Card(colors[i], 30 + 200 * x, 250 + 270 * y);
			cards[i].state = Card.CARD_CLOSE;
		}
	}

	@Override
	public void onDraw(Canvas canvas) {
		canvas.drawBitmap(backgroundImage, 0, 0, null);

		for (int i = 0; i < 6; i++) {
			Card card = cards[i];
			if (card.state == Card.CARD_CLOSE) {
				canvas.drawBitmap(backside, card.getX(), card.getY(), null);
			} else {
				if (card.color == Card.COLOR_BLUE) {
					canvas.drawBitmap(cardBlue, card.getX(), card.getY(), null);
				} else if (card.color == Card.COLOR_RED) {
					canvas.drawBitmap(cardRed, card.getX(), card.getY(), null);
				} else {
					canvas.drawBitmap(cardGreen, card.getX(), card.getY(), null);
				}

			}
		}

		Paint paint = new Paint();
		paint.setColor(Color.BLUE);
		paint.setTextSize(40);

		if (state == STATE_READY) {

			canvas.drawText("Touch anywhere to start game!", 20, 840, paint);
		} else if (state == STATE_GAME) {
			long time = (System.currentTimeMillis() - startedAt) / 1000;

			canvas.drawText("Time: " + time, 20, 900, paint);
		} else if (state == STATE_END) {
			long time = (endedAt - startedAt) / 1000;

			canvas.drawText("Congratulations!", 20, 840, paint);
			canvas.drawText("Elapsed time: " + time, 20, 900, paint);
		}
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (getState() == STATE_READY) {
			startGame();
		} else if (getState() == STATE_GAME) {
			if (selectedCard1 != null && selectedCard2 != null) {
				return true;
			}

			int px = (int) event.getX();
			int py = (int) event.getY();

			for (int i = 0; i < 6; i++) {
				Card card = cards[i];

				if (cards[i].isContains(px, py)) {
					if (card.state == Card.CARD_MATCHED) {
						// Don't need to flip out.
					} else if (selectedCard1 == null) {
						selectedCard1 = card;
						selectedCard1.state = Card.CARD_PLAYEROPEN;
						soundPool.play(soundFlipId, 1, 1, 0, 0, 1);
					} else {
						if (selectedCard1 != card) {
							selectedCard2 = card;
							selectedCard2.state = Card.CARD_PLAYEROPEN;
							soundPool.play(soundFlipId, 1, 1, 0, 0, 1);
						}
					}
				}
			}
		} else if (getState() == STATE_END) {
			setState(STATE_READY);
		}

		postInvalidate();
		return false;
	}

	public void startGame() {
		SetCardShuffle();
		Log.i("XXX", "Game start");
		setState(STATE_HINT);
	}

	public void endGame() {
		endTimer();

		setState(CardGameView.STATE_END);
	}

	public boolean isAllMatched() {
		for (Card card : cards) {
			if (card.state != Card.CARD_MATCHED) {
				return false;
			}
		}

		return true;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public void startTimer() {
		startedAt = System.currentTimeMillis();
		if (timer != null) {
			timer.cancel();
		}

		timer = new Timer(true);
		timer.scheduleAtFixedRate(new UpdateTimer(), 1000, 1000);
	}

	public void endTimer() {
		endedAt = System.currentTimeMillis();
		if (timer != null) {
			timer.cancel();
		}
	}

	private class UpdateTimer extends TimerTask {

		@Override
		public void run() {
			postInvalidate();
		}

	}

}
